package com.larade;

public class Client {

    private String nom;
    private String prenom;
    private int age;
    private int anewDePermit;

    public Client(String nom, String prenom, int age, int anneeDePermis) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.anewDePermit = anneeDePermis;
    }

    public String getNom() {
        return nom;
    }



    public String getPrenom() {
        return prenom;
    }



    public int getAge() {
        return age;
    }



    public int getAnneeDePermis() {
        return anewDePermit;
    }



    @Override
    public String toString() {
        return  nom +
                " " + prenom +  " " + age +
                ", anneeDePermis=" + anewDePermit
                ;
    }
}
