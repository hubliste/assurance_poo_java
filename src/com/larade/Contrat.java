package com.larade;

public class Contrat {

   private int ancienete;
   private int accident;

    public Contrat(int ancienete, int accident) {
        this.ancienete = ancienete;
        this.accident = accident;
    }

    public int getAncienete() {
        return ancienete;
    }

    public void setAncienete(int ancienete) {
        this.ancienete = ancienete;
    }

    public int getAccident() {
        return accident;
    }

    public void setAccident(int accident) {
        this.accident = accident;
    }



}
