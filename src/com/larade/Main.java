package com.larade;
/*
 *@author LARADE Jean-philippr
 *
 *
 * */
import java.util.Scanner;

public class Main {
    static Scanner clavier = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Entrez votre nom : ");
        String nomDuClient = clavier.nextLine();

        System.out.println("Entrez votre prénom : ");
        String prenomDuClient = clavier.nextLine();

        System.out.println("Entrez votre age : ");
        int ageDuClient = clavier.nextInt();

        System.out.println("Combien de temps avez vous votre permis?");
        int ageOfDriverLiscence = clavier.nextInt();

        System.out.println("Combien de temps de fidelité");
        int anneeDeFidelite = clavier.nextInt();

        System.out.println("Combien d 'accidents :");
        int nbrAccident = clavier.nextInt();
        Client nouveauClient  = new Client(nomDuClient,prenomDuClient,ageDuClient,ageOfDriverLiscence);
        Contrat nouveauContrat = new Contrat(anneeDeFidelite,nbrAccident);

         boolean jeunPermisansAccident = nouveauClient.getAge() <25 &
                 nouveauClient.getAnneeDePermis()<2 &
                 nouveauContrat.getAccident()==0 ;
                 //nouveauContrat.getAncienete()>5;

         /*boolean jeunePermisAvecAccident = nouveauClient.getAge() <25 &
                 nouveauClient.getAnneeDePermis()<2 &
                 nouveauContrat.getAccident()==0  &
                 nouveauContrat.getAncienete()<5;*/

        boolean jeuneVPermisansAccidentFidel = nouveauClient.getAge() <25 &
                nouveauClient.getAnneeDePermis()>2 &
                nouveauContrat.getAccident()==0
               ;

        boolean vieuxJPermisansAccidentFidel = nouveauClient.getAge() >25 &
                nouveauClient.getAnneeDePermis()<2 &
                nouveauContrat.getAccident()==0 ;

        boolean vieuxJPermisAvecUnAccident = nouveauClient.getAge() >25 &
                nouveauClient.getAnneeDePermis()<2 &
                nouveauContrat.getAccident()==1;

        boolean jeuneVPermisAvecUnAccident = nouveauClient.getAge() <25 &
                nouveauClient.getAnneeDePermis()>2 &
                nouveauContrat.getAccident()==1;

       boolean vieuxVPermisansAccident = nouveauClient.getAge() >25 &
               nouveauClient.getAnneeDePermis()>2 &
               nouveauContrat.getAccident()==0;

       boolean vieuxVPermisAvecUnAccident = nouveauClient.getAge() >25 &
               nouveauClient.getAnneeDePermis()>2 &
               nouveauContrat.getAccident()==1;

        boolean vieuxVPermisAvecDeuxAccident = nouveauClient.getAge() >25 &
                nouveauClient.getAnneeDePermis()>2 &
                nouveauContrat.getAccident()==2;


       if (jeuneVPermisansAccidentFidel ||vieuxJPermisansAccidentFidel || vieuxVPermisAvecUnAccident)
       {
           if(nouveauContrat.getAncienete()>5){
               couleurContrat(nouveauClient.getNom(),nouveauClient.getPrenom(),"VERT");
           }else{
               couleurContrat(nouveauClient.getNom(),nouveauClient.getPrenom(),"ORANGE");
           }
       }
       else if(vieuxJPermisAvecUnAccident ||
               jeuneVPermisAvecUnAccident ||
               vieuxVPermisAvecDeuxAccident ||
               jeunPermisansAccident)
       {
           if(nouveauContrat.getAncienete()>5){
               couleurContrat(nouveauClient.getNom(),nouveauClient.getPrenom(),"ORANGE");
           }else{
               couleurContrat(nouveauClient.getNom(),nouveauClient.getPrenom(),"ROUGE");
           }
       }
       else if(vieuxVPermisansAccident)
       {
           if(nouveauContrat.getAncienete()>5){
               couleurContrat(nouveauClient.getNom(),nouveauClient.getPrenom(),"BLEU");
           }else{
               couleurContrat(nouveauClient.getNom(),nouveauClient.getPrenom(),"VERT");
           }
       }else {
           System.out.println("Refus de la compagnie.");
       }

    }

    /*
    * methode  couleurContrat() pour afficher le nom ,prenoms et le couleur du Tarif
    *@param String nom
    *@param String Prenom
    *@param String couleur
    *
    * */

    public static void couleurContrat( String nom , String prenom, String c) {
        System.out.println(nom +"  "  + prenom+ "  " +"vous beneficiez du tarif "+c);
    }

}

